set :application, "phptoolbox"
set :domain,      "devzen.ru"
set :deploy_to,   "/var/www/kharybin.ru"
set :app_path,    "app"

set :use_sudo,    false
set :user,        "chainsmoker"

set :scm,         :git
set :repository,  "git@bitbucket.org:akharybin/phptoolbox.git"
set :deploy_via,   :copy

set :model_manager, "doctrine"

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server

set  :use_shared_files,    ["app/config/parameters.yml"]
set  :use_shared_children, [app_path + "/logs", app_path + "/cache", web_path + "/uploads"]

set  :use_composer, true
set  :update_vendors, true

set  :keep_releases,  3

task :upload_parameters do
  origin_file = "app/config/parameters.yml"
  destination_file = latest_release + "/app/config/parameters.yml" # Notice the
  latest_release

  try_sudo "mkdir -p #{File.dirname(destination_file)}"
  top.upload(origin_file, destination_file)
end

before "deploy:share_childs", "upload_parameters"

set :writable_dirs,       ["app/cache", "app/logs"]
set :webserver_user,      "www-data"
set :permission_method,   :acl
set :use_set_permissions, true

