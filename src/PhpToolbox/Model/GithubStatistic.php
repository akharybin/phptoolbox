<?php
namespace PhpToolbox\Model;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @Entity
 * @Table(name="github_statistic")
 */
class GithubStatistic
{
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     *
     * @var integer
     */
    private $id;

    /**
     * @Column(type="integer")
     *
     * @var integer
     */
    private $watchers;

    /**
     * @Column(type="integer")
     *
     * @var integer
     */
    private $forks;

    /**
     * @OneToOne(targetEntity="PhpToolbox\Model\Library", mappedBy="githubStatistic")
     *
     * @var Library
     */
    private $library;

    /**
     * @return int
     */
    public function getForks()
    {
        return $this->forks;
    }

    /**
     * @param int $forks
     */
    public function setForks($forks)
    {
        $this->forks = $forks;
    }

    /**
     * @return int
     */
    public function getWatchers()
    {
        return $this->watchers;
    }

    /**
     * @param int $watchers
     */
    public function setWatchers($watchers)
    {
        $this->watchers = $watchers;

    }

    public function getLibrary()
    {
        return $this->library;
    }
}
