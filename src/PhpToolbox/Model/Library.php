<?php
namespace PhpToolbox\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use PhpToolbox\Validator\GithubUrlFormat;
use PhpToolbox\Validator\LibraryNotAlreadyExists;
use PhpToolbox\Validator\RepositoryExists;
use PhpToolbox\Model\Category;

/**
 * @Entity
 * @Table(name="library")
 */
class Library
{
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     *
     * @var integer
     */
    private $id;

    /**
     * @GithubUrlFormat(groups="flow_create_library_step1")
     * @LibraryNotAlreadyExists(groups="flow_create_library_step1")
     * @RepositoryExists(groups="flow_create_library_step1")
     */
    private $githubUrl;

    /**
     * @Column(unique=true, nullable=false)
     *
     * @var string
     */
    private $fullName;

    /**
     * @Column(nullable=false)
     *
     * @var string
     */
    private $shortName;

    /**
     * @Column(type="text")
     *
     * @var string
     */
    private $description;

    /**
     * @Column(nullable=true)
     *
     * @var string
     */
    private $logotypeURL;

    /**
     * @ManyToOne(targetEntity="PhpToolbox\Model\Category", inversedBy="libraries")
     * @JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @var Category
     */
    private $category;

    /**
     * @OneToOne(targetEntity="PhpToolbox\Model\GithubStatistic", cascade={"persist", "remove"}, inversedBy="library")
     *
     * @var GithubStatistic
     */
    private $githubStatistic;

    /**
     * @Column(type="boolean")
     *
     * @var bool
     */
    private $status = false;

    /**
     * @return int id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGithubUrl()
    {
        return $this->getFullName() ? 'https://github.com/' . $this->getFullName() : '';
    }

    /**
     * @param string $url
     */
    public function setGithubUrl($url)
    {
        $this->githubUrl = $url;

        preg_match('/github.com\/([A-Za-z0-9-]+\/(?:[A-Za-z0-9-]+(?=\.git)|[A-Za-z0-9-]+)){1,1}/', $url, $matches);

        if (!isset($matches[1])) {
            return;
        }

        $this->fullName = $matches[1];
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getLogotypeURL()
    {
        return $this->logotypeURL;
    }

    /**
     * @param string $logotypeURL
     */
    public function setLogotypeURL($logotypeURL)
    {
        $this->logotypeURL = $logotypeURL;
    }

    /**
     * @return GithubStatistic
     */
    public function getGithubStatistic()
    {
        return $this->githubStatistic;
    }

    /**
     * @param GithubStatistic $githubStatistic
     */
    public function setGithubStatistic(GithubStatistic $githubStatistic)
    {
        $this->githubStatistic = $githubStatistic;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = (bool) $status;
    }
}
