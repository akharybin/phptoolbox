<?php
namespace PhpToolbox\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GithubStatisticUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('github:update-statistic')
            ->setDescription('Update watchers and forks statistic')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $statisticManager = $this->getContainer()->get('phptoolbox.github_statistic_manager');
        $statistics       = $statisticManager->getAll();

        foreach ($statistics as $statistic) {
            $statisticManager->update($statistic);
        }

        $this->getContainer()->get('doctrine.orm.entity_manager')->flush();
    }
}
