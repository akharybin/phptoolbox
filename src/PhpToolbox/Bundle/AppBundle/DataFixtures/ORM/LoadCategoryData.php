<?php
namespace PhpToolbox\Bundle\AppBundle\DataFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PhpToolbox\Model\Category;

class LoadCategoryData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $categories = [
            'Frameworks' => [
                'Web Development frameworks',
                'Testing frameworks',
            ],
            'Database Abstraction' => [
                'ORM',
                'ODM'
            ],
            'Comet' => [
                'Web Sockets'
            ],
            'Components' => [
                'Dependency Injection',
                'Routers',
            ],
        ];

        foreach ($categories as $categoryName => $subcategories) {
            $parent = new Category();
            $parent->setName($categoryName);

            foreach ($subcategories as $subcategoryName) {
                $subcategory = new Category();
                $subcategory->setParent($parent);
                $subcategory->setName($subcategoryName);
                $parent->add($subcategory);
            }

            $manager->persist($parent);
        }

        $manager->flush();
    }
}
