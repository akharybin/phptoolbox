<?php
namespace PhpToolbox\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class LibraryNotAlreadyExists extends Constraint
{
    public $message = 'Library already exists';

    public function validatedBy()
    {
        return 'phptoolbox.libraryNotAlreadyExists';
    }
}
