<?php
namespace PhpToolbox\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class RepositoryExists extends Constraint
{
    public $message = 'Repository not found';

    public function validatedBy()
    {
        return 'phptoolbox.repositoryExists';
    }
}
