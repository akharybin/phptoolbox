<?php
namespace PhpToolbox\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class GithubUrlFormat extends Constraint
{
    public $message = 'Wrong URL format';

    public function validatedBy()
    {
        return 'phptoolbox.githubUrlFormat';
    }
}
