<?php
namespace PhpToolbox\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JMS\DiExtraBundle\Annotation\Validator;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use PhpToolbox\Manager\LibraryManager;

/**
 * @Validator("phptoolbox.githubUrlFormat")
 */
class GithubUrlFormatValidator extends ConstraintValidator
{
    /**
     * @var LibraryManager
     */
    private $libraryManager;

    /**
     * @InjectParams({
     *     "libraryManager" = @Inject("phptoolbox.library_manager")
     * })
     *
     * @param LibraryManager $libraryManager
     */
    public function __construct(LibraryManager $libraryManager)
    {
        $this->libraryManager = $libraryManager;
    }

    public function validate($value, Constraint $constraint)
    {
        $value = $this->libraryManager->getFullNameFromGithubUrl($value);

        if (null === $value) {
            $this->context->addViolation($constraint->message);
        }
    }
}
