<?php
namespace PhpToolbox\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JMS\DiExtraBundle\Annotation\Validator;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use PhpToolbox\Manager\LibraryManager;

/**
 * @Validator("phptoolbox.repositoryExists")
 */
class RepositoryExistsValidator extends ConstraintValidator
{
    /**
     * @var LibraryManager
     */
    private $libraryManager;

    /**
     * @InjectParams({
     *     "libraryManager" = @Inject("phptoolbox.library_manager")
     * })
     *
     * @param LibraryManager $libraryManager
     */
    public function __construct(LibraryManager $libraryManager)
    {
        $this->libraryManager = $libraryManager;
    }

    public function validate($value, Constraint $constraint)
    {
        $libraryManager = $this->libraryManager;
        $library        = $libraryManager->create($value);

        if (!$library) {
            $this->context->addViolation($constraint->message);
        }
    }
}
