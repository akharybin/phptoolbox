<?php
namespace PhpToolbox\Manager;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use PhpToolbox\Service\GithubApiService;
use PhpToolbox\Model\Library;

/**
 * @Service("phptoolbox.category_manager")
 */
class CategoryManager
{
    const MODEL_NAME = 'Model:Category';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @InjectParams({
     *     "em" = @Inject("doctrine.orm.entity_manager")
     * })
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findAllAndCountLibraries()
    {
        return $this->em
            ->createQuery('SELECT c as category, COUNT(l.id) as countLibraries FROM ' . self::MODEL_NAME . ' c JOIN c.libraries l  WHERE l.status = true GROUP BY c.id')
            ->getResult();
        ;
    }

    public function findAll()
    {
        return $this->em
            ->getRepository(self::MODEL_NAME)
            ->findAll()
        ;
    }

    public function findOneById($id)
    {
        return $this->em
            ->getRepository(self::MODEL_NAME)
            ->findOneById($id)
        ;
    }
}
