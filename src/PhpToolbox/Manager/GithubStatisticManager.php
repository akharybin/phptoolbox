<?php
namespace PhpToolbox\Manager;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use PhpToolbox\Service\GithubApiService;
use PhpToolbox\Model\GithubStatistic;

/**
 * @Service("phptoolbox.github_statistic_manager")
 */
class StatisticManager
{
    const MODEL_NAME = 'Model:GithubStatistic';

    /**
     * @var GithubApiService
     */
    private $githubApiService;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @InjectParams({
     *     "githubApiService" = @Inject("phptoolbox.github_api_service"),
     *     "em"            = @Inject("doctrine.orm.entity_manager")
     * })
     */
    public function __construct(GithubApiService $githubApiService, EntityManager $em)
    {
        $this->githubApiService = $githubApiService;
        $this->em            = $em;
    }

    public function getAll()
    {
        return $this->em
            ->getRepository(self::MODEL_NAME)
            ->findAll();
    }

    public function findStatisticByLibraryFullName($fullName)
    {
        return $this->githubApiService->findStatisticByLibraryFullName($fullName);
    }

    public function update(GithubStatistic $statistic)
    {
        $updatedStatistic = $this->githubApiService->updateStatistic($statistic);
//@TODO ФЛАШИТЬ один раз!!!1111

        return $updatedStatistic;
    }

    public function save(GithubStatistic $statistic)
    {
        $this->em->persist($statistic);
        $this->em->flush();
    }
}
