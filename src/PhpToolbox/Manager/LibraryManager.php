<?php
namespace PhpToolbox\Manager;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use PhpToolbox\Service\GithubApiService;
use PhpToolbox\Model\Library;

/**
 * @Service("phptoolbox.library_manager")
 */
class LibraryManager
{
    const MODEL_NAME = 'Model:Library';

    /**
     * @var GithubApiService
     */
    private $githubApiService;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @InjectParams({
     *     "githubApiService" = @Inject("phptoolbox.github_api_service"),
     *     "em"            = @Inject("doctrine.orm.entity_manager")
     * })
     */
    public function __construct(GithubApiService $githubApiService, EntityManager $em)
    {
        $this->githubApiService = $githubApiService;
        $this->em            = $em;
    }

    public function findOneById($id)
    {
        return $this->em
            ->getRepository(self::MODEL_NAME)
            ->findOneById($id)
        ;
    }

    public function findLibrariesNumber()
    {
        return $this->em
            ->getRepository(self::MODEL_NAME)
            ->createQueryBuilder('l')
            ->select('COUNT(l.id)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function findByFullName($fullname)
    {
        return $this->githubApiService->findLibraryByFullname($fullname);
    }

    public function findOneByFullName($fullname)
    {
        return $this->em
            ->getRepository(self::MODEL_NAME)
            ->findOneByFullName($fullname)
        ;
    }

    public function findAll()
    {
        return $this->em
            ->getRepository(self::MODEL_NAME)
            ->findAll()
        ;
    }

    public function findActive()
    {
        return $this->em
            ->getRepository(self::MODEL_NAME)
            ->findByStatus(true)
        ;
    }

    public function findActiveByCategoryId($categoryId)
    {
        return $this->em
            ->getRepository(self::MODEL_NAME)
            ->createQueryBuilder('l')
            ->innerJoin('l.category', 'c')
            ->where('c.id = :id')
            ->andWhere('l.status = :status')
            ->setParameter('id', $categoryId)
            ->setParameter('status', 1)
            ->innerJoin('l.githubStatistic', 's')
            ->orderBy('s.watchers', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function create($githubUrl)
    {
        return $this->githubApiService->findLibraryByGithubUrl($githubUrl);
    }

    /**
     * @param string $githubUrl
     * @return bool
     */
    public function isAlreadyExists($githubUrl)
    {
        $fullName = $this->getFullNameFromGithubUrl($githubUrl);

        if (!$fullName) {
            return false;
        }

        $library = $this->findOneByFullName($fullName);

        if ($library) {
            return true;
        }

        return false;
    }

    public function getFullNameFromGithubUrl($githubUrl)
    {
        preg_match('/github.com\/([A-Za-z0-9-]+\/(?:[A-Za-z-]+(?=\.git)|[A-Za-z0-9-]+)){1,1}/', $githubUrl, $matches);

        if (!isset($matches[1])) {
            return null;
        }

        return $matches[1];
    }

    public function save(GithubLibrary $library)
    {
        $this->em->persist($library);
        $this->em->flush();
    }
}
