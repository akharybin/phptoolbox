<?php
namespace PhpToolbox\Service;

use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use Guzzle\Http\Client;
use PhpToolbox\Model\Library;
use PhpToolbox\Model\GithubStatistic;

/**
 * @Service("phptoolbox.github_api_service")
 */
class GithubApiService
{
    const GITHUB_API_BASE_URL = 'https://api.github.com';

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @InjectParams({
     *     "client" = @Inject("guzzle_client_service")
     * })
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $client->setBaseUrl(self::GITHUB_API_BASE_URL);
        $this->httpClient =  $client;
    }

    /**
     * @param string $fullname
     *
     * @return Library
     */
    public function findLibraryByFullname($fullName)
    {
        $response = $this->findRepositoryByFullName($fullName);

        if (!$response) {
            return null;
        }

        $library   = new Library($response['full_name']);
        $statistic = new GithubStatistic();
        $statistic->setWatchers($response['watchers_count']);
        $statistic->setForks($response['forks_count']);

        $library->setGithubStatistic($statistic);

        return $library;
    }

    /**
     * @param string $githubUrl
     *
     * @return Library|null
     */
    public function findLibraryByGithubUrl($githubUrl)
    {
        preg_match('/github.com\/([-0-9a-zA-Z]+\/[-0-9a-zA-Z]+){1,1}/', $githubUrl, $matches);

        if (!isset($matches[1])) {
            return null;
        }

        $library = $this->findLibraryByFullname($matches[1]);

        return $library;
    }

    /**
     * @param string $fullName
     *
     * @return string
     */
    private function findRepositoryByFullName($fullName)
    {
        try {
            $response = $this->httpClient->get('repos/' . $fullName)->send()->json();
        } catch (\Exception $e) {
            return null;
        }

        return $response;
    }

    /**
     * @param string $fullname
     *
     * @return string
     */
    public function findStatisticByLibraryFullName($fullname)
    {
        $response = $this->findRepositoryByFullName($fullname);

        if (!$response) {
            return null;
        }

        $statistic = new GithubStatistic();
        $statistic->setWatchers($response['watchers_count']);
        $statistic->setForks($response['forks_count']);

        return $statistic;
    }

    public function updateStatistic(GithubStatistic $statistic)
    {
        $fullName = $statistic->getLibrary()->getFullName();
        $response = $this->findRepositoryByFullName($fullName);

        $statistic->setWatchers($response['watchers_count']);
        $statistic->setForks($response['forks_count']);

        return $statistic;
    }
}
