<?php
namespace PhpToolbox\Form;

use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Symfony\Component\Form\FormTypeInterface;

class CreateLibraryFlow extends FormFlow
{
    /**
     * @var FormTypeInterface
     */
    protected $formType;

    public function setFormType(FormTypeInterface $formType)
    {
        $this->formType = $formType;
    }

    protected function loadStepsConfig()
    {
        return array(
            [
                'label' => 'GitHub library address',
                'type'  => $this->formType,
            ],
            [
                'label' => 'Library information',
                'type'  => $this->formType,
            ],
            [
                'label' => 'Confirmation',
                'type'  => $this->formType,
            ],
        );
    }

    public function getFormOptions($step, array $options = array())
    {
        $options = parent::getFormOptions($step, $options);

        $options['flowStep']           = $step;
        $options['cascade_validation'] = true;

        return $options;
    }

    public function getName()
    {
        return 'create_library';
    }
}
