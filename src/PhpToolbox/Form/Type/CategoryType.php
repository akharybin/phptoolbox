<?php
namespace PhpToolbox\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use PhpToolbox\Manager\CategoryManager;
use PhpToolbox\Model\Category;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'PhpToolbox\Model\Category',
        ]);
    }

    public function getName()
    {
        return 'category';
    }
}