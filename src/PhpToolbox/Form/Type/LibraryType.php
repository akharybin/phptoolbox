<?php
namespace PhpToolbox\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Callback;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\FormType;
use PhpToolbox\Manager\CategoryManager;

/**
 * @Service("phptoolbox.library_type")
 */
class LibraryType extends AbstractType
{
    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @InjectParams({
     *     "categoryManager" = @Inject("phptoolbox.category_manager")
     * })
     */
    public function __construct(CategoryManager $categoryManager)
    {
        $this->categoryManager = $categoryManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options, $em = null)
    {

        $categories = $this->categoryManager->findAll();

        switch ($options['flowStep']) {
            case 1:
                $builder->add('githubUrl', 'url', ['label' => 'GitHub url']);
                break;
            case 2:
                $builder->add('shortName', 'text', ['label' => 'Name'])
                    ->add('description')
                    ->add('category', 'entity', [
                        'class'    => 'PhpToolbox\Model\Category',
                        'property' => 'name',
                        'choices'  => $categories,
                        'property' => 'name'
                    ]);
                break;
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'flowStep'   => null,
            'data_class' => 'PhpToolbox\Model\Library',
        ));
    }

    public function getName()
    {
        return 'library';
    }
}
