<?php
namespace PhpToolbox\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use PhpToolbox\Service\GithubApiService;
use PhpToolbox\Model\Library;

class LibraryController extends Controller
{
    /**
     * @var GithubApiService
     */
    private $githubApiService;

    /**
     * @InjectParams({
     *     "githubApiService" = @Inject("phptoolbox.github_api_service"),
     * })
     */
    public function __construct(GithubApiService $githubApiService)
    {
        $this->githubApiService = $githubApiService;
    }

    /**
     * @Route("add", name="library.create")
     */
    public function addAction()
    {
        $formData = new Library();
        $flow     = $this->get('phptoolbox.form.flow.createLibrary');
        $flow->bind($formData);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);
            $formData->setGithubUrl($form->getData()->getGithubUrl());

            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                $statistic = $this->githubApiService->findStatisticByLibraryFullName($formData->getFullName());
                $formData->setGithubStatistic($statistic);

                $em = $this->getDoctrine()->getManager();
                $em->persist($formData);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    "Library '{$formData->getShortName()}' will be added after moderator approval."
                );

                return $this->redirect($this->generateUrl('main.index'));
            }
        }

        return $this->render('::Library/step' . $flow->getCurrentStepNumber() . '.html.twig', ['form' => $form->createView(), 'flow' => $flow]);
    }
}
