<?php
namespace PhpToolbox\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;

class IndexController extends Controller
{
    /**
     * @Route("admin", name="admin.index")
     */
    public function indexAction()
    {
        return $this->render('::Admin/index.html.twig');
    }
}