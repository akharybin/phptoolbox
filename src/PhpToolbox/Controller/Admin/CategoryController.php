<?php
namespace PhpToolbox\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use PhpToolbox\Model\Category;
use PhpToolbox\Manager\CategoryManager;
use PhpToolbox\Form\Type\CategoryType;

class CategoryController extends Controller
{
    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @InjectParams({
     *     "categoryManager" = @Inject("phptoolbox.category_manager")
     * })
     *
     * @param CategoryManager $categoryManager
     */
    public function __construct(CategoryManager $categoryManager)
    {
        $this->categoryManager = $categoryManager;
    }

    /**
     * @Route("admin/category", name="admin.category.index")
     */
    public function indexAction()
    {
        $categories = $this->categoryManager->findAll();

        return $this->render('::Admin/Category/index.html.twig', ['categories' => $categories]);
    }

    /**
     * @Route("admin/category/add", name="admin.category.add")
     */
    public function addAction(Request $request)
    {
        $category = new Category();
        $form     = $this->createForm(new CategoryType(), $category);

        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $category = $form->getData();
                $em       = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    "Category '{$category->getName()}' was added"
                );

                return $this->redirect($this->generateUrl('admin.category.index'));
            }
        }

        return $this->render('::Admin/Category/add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("admin/category/update/{id}", name="admin.category.update")
     */
    public function updateAction($id, Request $request)
    {
        $category = $this->categoryManager->findOneById($id);
        $form     = $this->createForm(new CategoryType(), $category);

        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $updatedCategory = $form->getData();
                $category->setName($updatedCategory->getName());

                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    "Category '{$category->getName()}' was updated"
                );

                return $this->redirect($this->generateUrl('admin.category.index'));
            }
        }


        return $this->render('::Admin/Category/update.html.twig', ['form' => $form->createView(), 'id' => $category->getId()]);
    }

    /**
     * @Route("admin/category/delete/{id}", name="admin.category.delete")
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function deleteAction($id)
    {
        $category = $this->categoryManager->findOneById($id);

        if (!$category) {
            $this->createNotFoundException('Category not found.');
        }

        $manager = $this->getDoctrine()->getManager();

        $manager->remove($category);
        $manager->flush();

        $this->get('session')->getFlashBag()->add(
            'notice',
            "Category '{$category->getName()}' was deleted"
        );

        return $this->redirect($this->generateUrl('admin.category.index'));
    }

    /**
     * @Route("admin/success", name="admin.success")
     */
    public function successAction()
    {
        return $this->render('::Admin/del.html.twig');
    }
}