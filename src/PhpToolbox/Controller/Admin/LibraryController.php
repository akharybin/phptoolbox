<?php
namespace PhpToolbox\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use PhpToolbox\Model\Category;
use PhpToolbox\Manager\LibraryManager;
use PhpToolbox\Manager\CategoryManager;
use PhpToolbox\Form\Type\LibraryType;

class LibraryController extends Controller
{
    /**
     * @var LibraryManager
     */
    private $libraryManager;

    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @InjectParams({
     *     "libraryManager"  = @Inject("phptoolbox.library_manager"),
     *     "categoryManager" = @Inject("phptoolbox.category_manager")
     * })
     *
     * @param LibraryManager $libraryManager
     * @param CategoryManager $categoryManager
     */
    public function __construct(LibraryManager $libraryManager, CategoryManager $categoryManager)
    {
        $this->libraryManager  = $libraryManager;
        $this->categoryManager = $categoryManager;
    }

    /**
     * @Route("admin/library", name="admin.library.index")
     */
    public function indexAction()
    {
        $libraries = $this->libraryManager->findAll();

        return $this->render('::Admin/Library/index.html.twig', ['libraries' => $libraries]);
    }

    /**
     * @Route("admin/library/add", name="admin.library.add")
     */
    public function addAction(Request $request)
    {
        $category = new Category();
        $form     = $this->createForm(new CategoryType(), $category);

        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $category = $form->getData();
                $em       = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    "Category '{$category->getName()}' was added"
                );

                return $this->redirect($this->generateUrl('admin.library.index'));
            }
        }


        return $this->render('::Admin/Library/add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("admin/library/update/{id}", name="admin.library.update")
     */
    public function updateAction($id, Request $request)
    {
        $library = $this->libraryManager->findOneById($id);
        $form     = $this->createForm(new LibraryType($this->categoryManager), $library, ['flowStep' => 2]);

        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $updatedLibrary = $form->getData();
                $library->setShortName($updatedLibrary->getShortName());
                $library->setCategory($updatedLibrary->getCategory());
                $library->setDescription($updatedLibrary->getDescription());

                $em = $this->getDoctrine()->getManager();
                $em->persist($library);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    "Library '{$library->getShortName()}' was updated"
                );

                return $this->redirect($this->generateUrl('admin.library.index'));
            }
        }

        return $this->render('::Admin/Library/update.html.twig', ['form' => $form->createView(), 'id' => $library->getId()]);
    }

    /**
     * @Route("admin/library/delete/{id}", name="admin.library.delete")
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function deleteAction($id)
    {
        $library = $this->libraryManager->findOneById($id);

        if (!$library) {
            $this->createNotFoundException('Library not found.');
        }

        $manager = $this->getDoctrine()->getManager();

        $manager->remove($library);
        $manager->flush();

        $this->get('session')->getFlashBag()->add(
            'notice',
            "library '{$library->getShortName()}' was deleted"
        );

        return $this->redirect($this->generateUrl('admin.library.index'));
    }

    /**
     * @Route("admin/library/activate/{id}", name="admin.library.activate")
     * @param $id
     */
    public function activateAction($id)
    {
        $library = $this->libraryManager->findOneById($id);

        if (!$library) {
            $this->createNotFoundException('Library not found.');
        }

        $library->setStatus(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($library);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'notice',
            "library '{$library->getShortName()}' was activated"
        );

        return $this->redirect($this->generateUrl('admin.library.index'));
    }

    /**
     * @Route("admin/success", name="admin.success")
     */
    public function successAction()
    {
        return $this->render('::Admin/del.html.twig');
    }
}