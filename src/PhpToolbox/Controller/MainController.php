<?php
namespace PhpToolbox\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Inject;
use PhpToolbox\Manager\LibraryManager;
use PhpToolbox\Manager\CategoryManager;

class MainController extends Controller
{
    /**
     * @var LibraryManager
     */
    private $libraryManager;

    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @InjectParams({
     *     "libraryManager"  = @Inject("phptoolbox.library_manager"),
     *     "categoryManager" = @Inject("phptoolbox.category_manager")
     * })
     *
     * @param LibraryManager $libraryManager
     * @param CategoryManager $categoryManager
     */
    public function __construct(LibraryManager $libraryManager, CategoryManager $categoryManager)
    {
        $this->libraryManager  = $libraryManager;
        $this->categoryManager = $categoryManager;
    }

    /**
     * @Route("/", name="main.index")
     */
    public function indexAction()
    {
        $categories = $this->categoryManager->findAllAndCountLibraries();

        return $this->render('::Main/index.html.twig', ['categories' => $categories]);
    }

    /**
     * @Route("/category/{id}", name="main.category")
     */
    public function categoryAction($id)
    {
        $category  = $this->categoryManager->findOneById($id);
        $libraries = $this->libraryManager->findActiveByCategoryId($id);

        return $this->render('::Main/library.html.twig', ['libraries' => $libraries, 'category' => $category]);
    }

    /**
     * @Route("/login")
     */
    public function login()
    {
        return $this->render('::Main/login.html.twig');
    }
}
