$(document).ready(function () {
    $('.library-description').jTruncate({
        length: 100,
        minTrail: 0,
        moreText: "[more]",
        lessText: "[less]",
        ellipsisText: "...",
        moreAni: "fast",
        lessAni: 200
    });
});